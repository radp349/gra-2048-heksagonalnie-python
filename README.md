# Gra 2048 Heksagonalnie

Hexagonal 2048 to innowacyjna wariacja na temat popularnej gry logicznej 2048, zaprojektowana w oparciu o pola sześciokątne zamiast kwadratowych. Ta zaawansowana aplikacja umożliwia rozgrywkę dla jednego gracza, jak również tryb gry dwuosobowej.

## Instalacja

Aby prawidłowo zainstalować i uruchomić Hexagonal 2048, należy wykonać następujące kroki:

1. Sklonuj repozytorium za pomocą polecenia: `git clone https://github.com/TwojGithub/Hexagonal-2048.git`
2. Przejdź do katalogu Hexagonal 2048 używając: `cd Hexagonal-2048`
3. Zainstaluj wymagane pakiety za pomocą: `npm install`

## Zasady gry 

Hexagonal 2048 bazuje na klasycznej grze 2048, ale wprowadza do niej dodatkowy kierunek ruchu płytek. Twoim celem jest łączenie płytek o tej samej wartości na sześciokątnej planszy, aż osiągniesz liczbę 2048.

## Kluczowe funkcjonalności

1. **Sterowanie za pomocą przycisków**: Dzięki intuicyjnemu sterowaniu przy pomocy przycisków odnoszących się do wartości zegara, możesz przesuwać wszystkie dostępne płytki w wybranym kierunku, starając się osiągnąć jak najwyższy wynik.

2. **Tryb automatycznej rozgrywki**: Hexagonal 2048 oferuje unikalną funkcję autonomatycznej rozgrywki, pozwalającą na automatyczne przeprowadzenie części gry. Jest to świetny sposób aby móc przyspieszyć grę i zacząć z dalszego etapu.

3. **Tryb gry wieloosobowej**: Hexagonal 2048 umożliwia rywalizację dwóch graczy w trybie lokalnym. Gracze konkurują na indywidualnych planszach, dążąc do osiągnięcia jak najwyższego wyniku.



from PySide2.QtWidgets import QGraphicsItem, QGraphicsScene, QApplication, QMainWindow, QGraphicsView, QLabel, QGraphicsTextItem, QPushButton, QTextEdit, QLineEdit, QMessageBox
from PySide2.QtGui import QPen, QBrush, Qt, QPolygon, QColor, QFont
from PySide2.QtCore import QPoint, QProcess
import math
import random
import sys
import socket
import threading
import time


class Pole(QGraphicsItem):
    def __init__(self, rozmiar_pola, upxx, upyy):
        super().__init__()
        self.start_values = [2,2,2,4]
        self.Values = [None,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384]
        self.Foreground  = [QBrush(QColor(255,255,255)),QBrush(QColor(66,192,66)),QBrush(QColor(0,99,66)),QBrush(QColor(0,99,255)),QBrush(QColor(255,99,0)),QBrush(QColor(255,33,33)),
        QBrush(QColor(255,33,106)),QBrush(QColor(255,99,192)),QBrush(QColor(192,33,255)),QBrush(QColor(255,192,0)),QBrush(QColor(85,94,42)),
        QBrush(QColor(32,99,192)),QBrush(QColor(170,23,116)),QBrush(QColor(61,61,61)),QBrush(QColor(192,0,192))]
        self.sqrt3 = math.sqrt(3)
        self.font = QFont()
        self.length = rozmiar_pola
        self.granted = False
        self.upx, self.upy = self.Ustaw_pozycje(upxx,upyy)
        self.tekst = QGraphicsTextItem(self)
        self.value = self.Pobierz_numer()     
        self.coords = self.Pobierz_pole(self.upx,self.upy)
        self.BlackBorder = QPen(Qt.black, 8, Qt.SolidLine)
        self.Back = self.Foreground[self.Values.index(self.value)]
        

        

    # Metoda generująca pole jako zbiór punków z przypisanym nuerem (wartością tego pola)
    def Pobierz_pole(self, upxx, upyy):
        self.Ustaw_pozycje(upxx,upyy)
        self.coords = QPolygon([
            QPoint(0 + self.upx, 0.5*self.length + self.upy),
            QPoint(0 + self.upx, 1.5*self.length + self.upy),
            QPoint((0.5*self.length*self.sqrt3) + self.upx, 2*self.length + self.upy),
            QPoint((self.length*self.sqrt3) + self.upx, 1.5*self.length + self.upy),
            QPoint((self.length*self.sqrt3) + self.upx, 0.5*self.length + self.upy),
            QPoint((0.5*self.length*self.sqrt3) + self.upx, 0 + self.upy)
        ])
        self.Ustaw_numer()
        
        return self.coords

    # Metoda przypisująca polu wartość 2 lub 4 (losowo)
    def Pobierz_numer(self):
        self.granted = True
        self.value = self.start_values[random.randint(0,3)]
        self.Ustaw_numer()

    # Metoda przypisująca polu artość 2
    def Pobierz_numer2(self):
        self.granted = True
        self.value = 2
        self.Ustaw_numer()

    # Metoda pozbawiająca pole wartości
    def Zeruj(self):
        self.granted = False
        self.value = None
        self.Ustaw_numer()

    # Metoda określająca polu wyisywalny tekt, będący jego wartością
    def Ustaw_numer(self):

        self.Back = self.Foreground[self.Values.index(self.value)]


        if(self.value != None):

            if(self.value > 900):
                self.font.setPixelSize(int(self.length/3.6))
                self.tekst.setHtml('<h1 style="color:black";>'f"{self.value}"'</h1>')
                self.tekst.setFont(self.font)
                self.tekst.setPos(0.55*self.length+self.upx - (int(self.length/5.5)*(len(str(self.value)) - 1)),0.45*self.length+self.upy)
            else:
                self.font.setPixelSize(int(self.length/3))
                self.tekst.setHtml('<h1 style="color:black";>'f"{self.value}"'</h1>')
                self.tekst.setFont(self.font)
                self.tekst.setPos(0.55*self.length+self.upx - (int(self.length/5)*(len(str(self.value)) - 1)),0.45*self.length+self.upy)

        else:
            self.font.setPixelSize(int(self.length/3))
            self.tekst.setHtml('<h1 style="color:black";>'f""'</h1>')
            self.tekst.setFont(self.font)
            self.tekst.setPos(0.55*self.length+self.upx - (int(self.length/6)*(len(str(self.value)) - 1)),0.45*self.length+self.upy) 
        return self.tekst          

    # Metoda definiująca pozycję pola w oknie
    def Ustaw_pozycje(self,upxx,upyy):
        self.upx = upxx
        self.upy = upyy
        return upxx, upyy


        

class Okno(QMainWindow):
    def __init__(self):
        super().__init__()

        self.sceny= []
        self.plansze = []
        self.sqrt3 = math.sqrt(3)

        # Atrybuty do gry sieciowej
        self.dwuosobowa = False
        self.ruch = True
        self.serwer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serwer_adres = ('localhost', 10000)
        self.connections = []
        self.receive_threads = []
        self.connected = False

        # Autorozgrywka

        self.auto = False
        self.step = 100



        # Listy uzależniające kolor pola od jego wartości 
        self.Values = [None,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384]
        self.Foreground  = [QBrush(QColor(255,255,255)),QBrush(QColor(66,192,66)),QBrush(QColor(0,99,66)),QBrush(QColor(0,99,255)),QBrush(QColor(255,99,0)),QBrush(QColor(255,33,33)),
        QBrush(QColor(255,33,106)),QBrush(QColor(255,99,192)),QBrush(QColor(192,33,255)),QBrush(QColor(255,192,0)),QBrush(QColor(85,94,42)),
        QBrush(QColor(32,99,192)),QBrush(QColor(170,23,116)),QBrush(QColor(61,61,61)),QBrush(QColor(192,0,192))]
        self.setWindowTitle("2048 Heksagonalnie")

        # Atrybuty ustalające wielkość pola i rozmiar planszy - domyślnie 3x3x3
        self.rozmiar_pola = 40
        self.rozmiar_planszy = 3
        self.gap = 12
        self.BlackBorder = QPen(Qt.black, 8, Qt.SolidLine)
        self.setGeometry(50,100,1540,920)
        self.rozklad = []
        self.ile_pol()
        self.odstep = [(self.rozmiar_planszy-1)]
        self.odstepy()

        # Atrybuty dotyczące warunku zwycięstwa
        self.score = 0
        self.finish = False
        self.result = 0

        self.scoretekst = QGraphicsTextItem()
        self.resulttekst = QGraphicsTextItem()
        self.scorefont = QFont()

        self.kolory = ['\033[94m','\033[96m','\033[92m','\033[93m']

        self.scorefont.setPixelSize(16)
        self.scoretekst.setHtml('<h1 style="color:blue";>'f"Score: {self.score}"'</h1>')
        self.resulttekst.setHtml('<h1 style="color:green";>'f""'</h1>')
        self.scoretekst.setFont(self.scorefont)
        self.resulttekst.setFont(self.scorefont)
        self.scoretekst.setPos(0,-100)
        self.resulttekst.setPos(0,-70)

        # Listy list, które przechowują pola ułożone w odpowiedniej kolejności (potrzebne do wykonywania ruchów)
        self.poziome = []
        self.slash = []
        self.backslash = []
        self.poziomeinv = []
        self.slashinv = []
        self.backslashinv = []

        self.kierunki = [self.poziome, self.slash, self.backslash, self.poziomeinv, self.slashinv, self.backslashinv]

    
        self.label = QLabel(self)
        self.label.move(10,100)

        self.process = QProcess(self)
        self.process.setProgram("dirb")
        self.process.setProcessChannelMode(QProcess.MergedChannels)
        self.process.readyReadStandardOutput.connect(self.Wypisz_historie)
        self.lineedit = QLineEdit("http://webscantest.com")

        

        self.textedit = QTextEdit(readOnly=True)
        self.textedit.setGeometry(140 + (2.2*self.rozmiar_planszy * self.rozmiar_pola), -170, self.rozmiar_planszy*55, self.rozmiar_planszy*55)
        self.textedit.setFont(self.scorefont)
        
        self.AddButtons()

        #self.Dodaj_scene_plansza()
        self.Dodaj_scene_plansza2()

        self.show()




# Metody określające rozłożenie pól na plansz, w zależności od jej rozmiau (potrzebne do wyrysowania planszy)
    def ile_pol(self):
        for i in range(self.rozmiar_planszy):
            self.rozklad.append(self.rozmiar_planszy + i)
        for i in range(self.rozmiar_planszy - 1):
            self.rozklad.append(self.rozklad[len(self.rozklad)-1]-1)
        return self.rozklad

    def odstepy(self):
        for i in range(self.rozmiar_planszy-1):
            self.odstep.append(self.odstep[len(self.odstep)-1]-1)
        for i in range(self.rozmiar_planszy-1):
            self.odstep.append(self.odstep[len(self.odstep)-1]+1)
        return self.odstep



# Metoda tworząca scenę i planszę
    def Dodaj_scene_plansza(self):
        plansza = []
        self.plansze.append(plansza)
        self.scena = QGraphicsScene()
        self.sceny.append(self.scena)
        self.widokgry = QGraphicsView(self.scena,self)
        self.widokgry.setGeometry(100,100,900 +70*self.rozmiar_planszy,600 + 70*self.rozmiar_planszy)

    
        self.scena.addWidget(self.textedit)

        self.Utworz_Plansze(plansza,0)
        self.Wyrysuj_Plansze(plansza)

# Metoda tworząca scenę i dwie plansze
    def Dodaj_scene_plansza2(self):
        planszaA = []
        planszaB = []
        self.plansze.append(planszaA)
        self.plansze.append(planszaB)
        self.scena = QGraphicsScene()
        self.sceny.append(self.scena)
        self.widokgry = QGraphicsView(self.scena,self)
        self.widokgry.setGeometry(100,100,900 +70*self.rozmiar_planszy,600 + 70*self.rozmiar_planszy)

    
        self.scena.addWidget(self.textedit)

        self.Utworz_PlanszeA(planszaA, 0)
        self.Utworz_PlanszeB(planszaB, 500)
        self.Wyrysuj_Plansze(planszaB)
        


# Metoda tworząca planszę (poprzez dodanie do niej pól i posegregowanie ich)
    def Utworz_PlanszeA(self, plansza, przesuniecie):
        which = 0

        for k in range(2*self.rozmiar_planszy - 1):
            slashx = []
            self.slash.append(slashx)


            backslashx = []
            self.backslash.append(backslashx)


        for i in range (2*self.rozmiar_planszy - 1):
            which = 0
            poziomx = []
            self.poziome.append(poziomx)

            for j in range(self.rozklad[i]):
                pole = Pole(self.rozmiar_pola, przesuniecie + (0.5*self.odstep[i]*(self.gap + self.rozmiar_pola*self.sqrt3)) + which*(self.gap + self.rozmiar_pola*self.sqrt3), 0 + i*(self.rozmiar_pola*self.sqrt3 - 2 + self.gap/3) )

                plansza.append(pole)
                self.poziome[i].append(pole)
                which += 1

                if (i < self.rozmiar_planszy):
                    self.slash[j].append(pole)
                else:
                    self.slash[(1 + i - self.rozmiar_planszy + j )].append(pole)

                if (i >= (self.rozmiar_planszy-1)):
                    self.backslash[j].append(pole)
                else:
                    self.backslash[(self.rozmiar_planszy - i - 1 + j )].append(pole)

        for lista in self.poziome:
            listainv = lista[::-1]
            self.poziomeinv.append(listainv)

        for lista in self.slash:
            listainv = lista[::-1]
            self.slashinv.append(listainv)

        for lista in self.backslash:
            listainv = lista[::-1]
            self.backslashinv.append(listainv)


    def Utworz_PlanszeB(self, plansza, przesuniecie):
        for i in range (2*self.rozmiar_planszy - 1):
            which = 0

            for j in range(self.rozklad[i]):
                pole = Pole(self.rozmiar_pola, przesuniecie + (0.5*self.odstep[i]*(self.gap + self.rozmiar_pola*self.sqrt3)) + which*(self.gap + self.rozmiar_pola*self.sqrt3), 0 + i*(self.rozmiar_pola*self.sqrt3 - 2 + self.gap/3) )
                plansza.append(pole)

                which += 1






# Metoda rysująca planszę (Ale tylko tuż po jej utworzeniu)
    def Wyrysuj_Plansze(self, plansza):
        for plansza in self.plansze:
            for pole in plansza:
                self.scena.addPolygon(pole.coords,pole.BlackBorder, pole.Back)
                self.scena.addItem(pole.tekst)
        self.scena.addItem(self.scoretekst)
        self.scena.addItem(self.resulttekst)

# Metoda aktualizująca planszę (Poza pierwszym wyrysowaniem)
    def Aktualizuj_Plansze(self, plansza):
        self.scena.update()
        for pole in plansza:
            self.scena.addPolygon(pole.coords,pole.BlackBorder, pole.Back)
            self.scena.removeItem(pole.tekst)
            self.scena.addItem(pole.tekst)
        self.scena.removeItem(self.scoretekst)
        self.scoretekst.setHtml('<h1 style="color:blue";>'f"Score: {self.score}"'</h1>')
        self.scena.addItem(self.scoretekst)

        if(self.result == 2):
            print("koniec")
    

        if((self.finish == True) and (self.result == 1)):
            self.scena.removeItem(self.resulttekst)
            self.resulttekst.setHtml('<h1 style="color:green";>'f"Zwycięstwo!"'</h1>')
            self.scena.addItem(self.resulttekst)

        

        elif((self.finish == True) and (self.result == 2)):
            self.scena.removeItem(self.resulttekst)
            self.resulttekst.setHtml('<h1 style="color:red";>'f"Koniec Gry!"'</h1>')
            self.scena.addItem(self.resulttekst)

        else:
            self.scena.removeItem(self.resulttekst)
            self.resulttekst.setHtml('<h1 style="color:green";>'f""'</h1>')
            self.scena.addItem(self.resulttekst)            

        self.scena.update()

# Metoda wypisują w konsoli historię gry przy pomocy losowych kolorów
    def Wypisz_historie(self,plansza):
        x = 0
        y = self.kolory[random.randint(0,len(self.kolory)-1)]
        end = '\033[0m'
        self.textedit.clear()
        for i in range (2*self.rozmiar_planszy - 1):
            
            string = ""
            stringedit = ""
            string += f"{y}"
            #if (i==0):
                #print("\n")     
            for k in range(self.odstep[i]):
                string += " "
                stringedit += "  "

            for j in range(self.rozklad[i]):
                if(plansza[x].value != None):
                    string += f"{plansza[x].value} "
                    stringedit += f"{plansza[x].value}  "
                else:
                    string += "# "
                    stringedit += "#  "
                x += 1
            string += f"{end}"
            # Wyłączam ypisywanie historii gry w konsoli
            #print(string)

            self.textedit.append(stringedit)



            
        
# Metoda dodająca do siebie sąsiadujące pola, podczas wykonywania ruchu
    def Dodawanie(self, pole1, pole2):
        pole2.value += pole1.value
        self.score += pole2.value
        pole2.granted = True
        pole2.Ustaw_numer()
        pole1.Zeruj()

    def Symuluj_Dodawanie(self, pole1, pole2):
        pole2.value += pole1.value
        self.autoscore += pole2.value
        pole2.granted = True
        pole2.Ustaw_numer()
        pole1.Zeruj()



# Metoda przerzucająca puste pola na koniec listy
    def Zamiana(self, pole1, pole2):
        bufor = pole1.value
        pole1.value = pole2.value
        pole2.value = bufor

        pole1.granted = False
        pole2.granted = True

        pole1.Ustaw_numer()
        pole2.Ustaw_numer()

# Metoda spradzająca warunek zwycięstwa
    def Czy_koniec(self, plansza):
        
        plus2048 =[]
        length = len(plansza)
        for j in range(0,length):
            if(plansza[j].value != None):
                if(plansza[j].value >= 16384):
                    plus2048.append(j)

        if((len(plus2048)) > 0):
            self.finish = True
            self.result = 1
            self.auto = False
            
# Metoda wykonująca ruch we wskazanym kierunku
    def Wykonaj_Ruch(self, kierunek):

        czy_zmiana = 0
        for iteracje in range (max(self.rozklad)-1):
            for lista in kierunek:
                i = 0
                for pole in lista:
                    if(i != 0):
                        if (pole.value == None):
                            if (lista[i-1].value != None):
                                #self.Zamiana(lista[i-1], pole)

                                bufor = pole.value
                                pole.value = lista[i-1].value
                                lista[i-1].value = bufor

                                pole.granted = False
                                lista[i-1].granted = True

                                pole.Ustaw_numer()
                                lista[i-1].Ustaw_numer()

                                czy_zmiana += 1
                                

                    i+=1

        for lista in kierunek:
            i = 0
            skip = 0
            for pole in lista:
                already_added = 0
                if(skip != 1):
                    if(i != (len(lista)-1)):
                        if pole.value != None:
                            if (i != (len(lista)-2)):
                                if ((pole.value == lista[i+1].value) and (pole.value == lista[i+2].value)):
                                    #self.Dodawanie(pole,lista[i+2])

                                    lista[i+2].value += pole.value
                                    self.score += lista[i+2].value
                                    lista[i+2].granted = True
                                    lista[i+2].Ustaw_numer()
                                    pole.Zeruj()

                                    skip = 2
                                    czy_zmiana += 1
                                    already_added +=1
                            if(already_added == 0):
                                if (pole.value == lista[i+1].value):
                                    #self.Dodawanie(pole,lista[i+1])

                                    lista[i+1].value += pole.value
                                    self.score += lista[i+1].value
                                    lista[i+1].granted = True
                                    lista[i+1].Ustaw_numer()
                                    pole.Zeruj()

                                    skip = 2
                                    czy_zmiana += 1
                skip -= 1
                i+=1

        for iteracje in range (max(self.rozklad)-1):
            for lista in kierunek:
                i = 0
                for pole in lista:
                    if(i != 0):
                        if (pole.value == None):
                            if (lista[i-1].value != None):
                                #self.Zamiana(lista[i-1], pole)

                                bufor = pole.value
                                pole.value = lista[i-1].value
                                lista[i-1].value = bufor

                                pole.granted = False
                                lista[i-1].granted = True

                                pole.Ustaw_numer()
                                lista[i-1].Ustaw_numer()

                                czy_zmiana += 1
                                

                    i+=1


        if (czy_zmiana != 0):
            self.Add_Value(self.plansze[0],1)
            self.ruch = False
            self.Wypisz_historie(self.plansze[0])

        self.Czy_koniec(self.plansze[0])

        self.Aktualizuj_Plansze(self.plansze[0])

        return czy_zmiana



    def Oszacuj_Ruch(self, kierunek):

        czy_zmiana = 0
        self.autoscore = 0

        Zapis = []
        Granted = []

        

        for pole in self.plansze[0]:
            Zapis.append(pole.value)
            Granted.append(pole.granted)

        for iteracje in range (max(self.rozklad)-1):
            for lista in kierunek:
                i = 0
                for pole in lista:
                    if(i != 0):
                        if (pole.value == None):
                            if (lista[i-1].value != None):
                                #self.Zamiana(lista[i-1], pole)

                                bufor = pole.value
                                pole.value = lista[i-1].value
                                lista[i-1].value = bufor

                                pole.granted = False
                                lista[i-1].granted = True

                                pole.Ustaw_numer()
                                lista[i-1].Ustaw_numer()

                                czy_zmiana += 1
                                

                    i+=1

        for lista in kierunek:
            i = 0
            skip = 0
            for pole in lista:
                already_added = 0
                if(skip != 1):
                    if(i != (len(lista)-1)):
                        if pole.value != None:
                            if (i != (len(lista)-2)):
                                if ((pole.value == lista[i+1].value) and (pole.value == lista[i+2].value)):
                                    
                                    #self.Symuluj_Dodawanie(pole,lista[i+2])
                                    lista[i+2].value += pole.value
                                    self.autoscore += lista[i+2].value

                                    skip = 2
                                    czy_zmiana += 1
                                    already_added +=1
                            if(already_added == 0):
                                if (pole.value == lista[i+1].value):
                                    #self.Symuluj_Dodawanie(pole,lista[i+1])
                                    lista[i+1].value += pole.value
                                    self.autoscore += lista[i+1].value
                                    skip = 2
                                    czy_zmiana += 1
                skip -= 1
                i+=1

        it=0
        for pole in self.plansze[0]:
            pole.value = Zapis[it]
            pole.granted = Granted[it]
            pole.Ustaw_numer()
            it+=1



        Zapis.clear()
        Granted.clear()
            

        self.Czy_koniec(self.plansze[0])

        self.Aktualizuj_Plansze(self.plansze[0])



        return czy_zmiana, self.autoscore

# Metoda dodająca przyciski do okna
    def AddButtons (self):

        self.button1 = QPushButton(self)
        self.button1.setText("1")
        self.button1.resize(self.button1.minimumSizeHint())
        self.button1.move(140,0)
        self.button1.clicked.connect(self.Dodaj_na1)

        button3 = QPushButton(self)
        button3.setText("3")
        button3.resize(button3.minimumSizeHint())
        button3.move(180,30)
        button3.clicked.connect(self.Dodaj_na3)

        button5 = QPushButton(self)
        button5.setText("5")
        button5.resize(button5.minimumSizeHint())
        button5.move(140,60)
        button5.clicked.connect(self.Dodaj_na5)

        button7 = QPushButton(self)
        button7.setText("7")
        button7.resize(button7.minimumSizeHint())
        button7.move(50,60)
        button7.clicked.connect(self.Dodaj_na7)

        button9 = QPushButton(self)
        button9.setText("9")
        button9.resize(button9.minimumSizeHint())
        button9.move(10,30)
        button9.clicked.connect(self.Dodaj_na9)

        button11 = QPushButton(self)
        button11.setText("11")
        button11.resize(button11.minimumSizeHint())
        button11.move(50,0)
        button11.clicked.connect(self.Dodaj_na11)

        buttonnew = QPushButton(self)
        buttonnew.setText("Nowa Gra")
        buttonnew.resize(buttonnew.minimumSizeHint())
        buttonnew.move(300,20)
        buttonnew.clicked.connect(self.Restart)


        buttonexit = QPushButton(self)
        buttonexit.setText("Wyjście")
        buttonexit.resize(buttonexit.minimumSizeHint())
        buttonexit.move(300,50)
        buttonexit.clicked.connect(self.Exit)


        buttonsave = QPushButton(self)
        buttonsave.setText("Zapisz Historię Gry")
        buttonsave.resize(buttonsave.minimumSizeHint())
        buttonsave.move(400,20)

        buttonemu = QPushButton(self)
        buttonemu.setText("Emuluj")
        buttonemu.resize(buttonemu.minimumSizeHint())
        buttonemu.move(400,50)

        buttonauto = QPushButton(self)
        buttonauto.setText("Autorozgrywka - Wykonaj "f"{self.step}"" ruchów")
        buttonauto.resize(buttonauto.minimumSizeHint())
        buttonauto.move(520,20)
        buttonauto.clicked.connect(self.Autorozgrywka)

        button2ps = QPushButton(self)
        button2ps.setText("Gra  Dwuosobowa - Utwórz Serwer")
        button2ps.resize(button2ps.minimumSizeHint())
        button2ps.move(520,50)
        button2ps.clicked.connect(self.MultiplayerSerwer)

        button2pc = QPushButton(self)
        button2pc.setText("Gra  Dwuosobowa - Dołącz do gry")
        button2pc.resize(button2pc.minimumSizeHint())
        button2pc.move(720,50)
        button2pc.clicked.connect(self.MultiplayerKlient)



# Metody inicjujące wykonanie ruchu, po naciśnięciu przycisków 1-11
    def Dodaj_na1 (self):
        if(self.finish == False):
            if(not((self.dwuosobowa == True) and (self.ruch == False))):      
                self.Wykonaj_Ruch(self.slashinv)

    def Dodaj_na3 (self):
        if(self.finish == False):
            if(not((self.dwuosobowa == True) and (self.ruch == False))): 
                self.Wykonaj_Ruch(self.poziome)
    

    def Dodaj_na5 (self):
        if(self.finish == False):
            if(not((self.dwuosobowa == True) and (self.ruch == False))): 
                self.Wykonaj_Ruch(self.backslash)

    def Dodaj_na7 (self):
        if(self.finish == False):
            if(not((self.dwuosobowa == True) and (self.ruch == False))): 
                self.Wykonaj_Ruch(self.slash)

    def Dodaj_na9 (self):
        if(self.finish == False):
            if(not((self.dwuosobowa == True) and (self.ruch == False))): 
                self.Wykonaj_Ruch(self.poziomeinv)

    def Dodaj_na11 (self):
        if(self.finish == False):
            if(not((self.dwuosobowa == True) and (self.ruch == False))): 
                self.Wykonaj_Ruch(self.backslashinv)


    def Autorozgrywka (self):

        # self.kierunki = [self.poziome, self.slash, self.backslash, self.poziomeinv, self.slashinv, self.backslashinv]
        # result = self.Wykonaj_Ruch(self.kierunki[random.randint(0,len(self.kierunki)-1)])

        direction = [2,1,0]
        self.auto = True
        self.over = 0
        maxgain = 0
        whichismax = 0
        it = 0

        while self.auto:
            maxgain = 0


            for i in range(len(direction)):
                result, gain = self.Oszacuj_Ruch(self.kierunki[direction[i]])
                if(gain > maxgain):
                    maxgain = gain
                    whichismax = direction[i]
                    

            if(maxgain != 0):
                result = self.Wykonaj_Ruch(self.kierunki[whichismax])

            else: 
                result = self.Wykonaj_Ruch(self.kierunki[3])

                if (result == 0):
                    result = self.Wykonaj_Ruch(self.kierunki[2])

                if (result == 0):
                    result = self.Wykonaj_Ruch(self.kierunki[1])

                if (result == 0):
                    result = self.Wykonaj_Ruch(self.kierunki[0])

                if (result == 0):
                    result = self.Wykonaj_Ruch(self.kierunki[4])

                if (result == 0):
                    result = self.Wykonaj_Ruch(self.kierunki[5])

                if (result == 0):
                    self.over = 1

            if((self.step - it) % (0.01*self.step) == 0):
                print("Wykonano " + str(100 - ((self.step - it) / (0.01*self.step))) + " %" + " symulacji")
                

            it +=1

            if self.over == 1:
                self.finish = True 
                self.result = 2
                self.auto = False
                break

            if(it > self.step):
                break

        self.Aktualizuj_Plansze(self.plansze[0])
            
            

        



    def MultiplayerSerwer(self):
        self.RestartMultiplayer()
        self.Utworz_serwer()
        

    def MultiplayerKlient(self):
        self.RestartMultiplayer()
        self.Wyszukaj_Serwer()

    


    def Utworz_serwer(self):
        try:
            self.serwer.bind(self.serwer_adres)
            self.n = 1
            self.serwer.listen(self.n)
            self.Komunikacja()
        except socket.error:
            print("error")
            return

    def Komunikacja(self):
        QMessageBox.about(self, "Sukces",'Pomyślnie utworzono serwer!')

        QMessageBox.about(self, "Waiting","Oczekanie na połączenie ("+str(1)+") użytkowników")
        connection, client_address = self.serwer.accept()
        print("Info: Klient właśnie połączył się z serwerem!")
        self.connections.append(connection)
        self.connected = True
        thread  = threading.Thread(target=self.Serwer_odbior, args=(self.connections[0],))
        thread.daemon = True
        thread.start()

        threadch  = threading.Thread(target=self.Czat_z_klientem)
        threadch.daemon = True
        threadch.start()

        self.receive_threads.append(thread)
        self.setWindowTitle("2048 Heksagonalnie - Serwer ")


    def Wyszukaj_Serwer(self):
        try:
            self.serwer.connect(self.serwer_adres)
            print("Info: Połączono z serwerem!")
            self.connected = True
            
            thread = threading.Thread(target=self.Klient_odbior)
            thread.daemon = True
            thread.start()

            threads  = threading.Thread(target=self.Czat_z_serwerem)
            threads.daemon = True
            threads.start()
            self.setWindowTitle("2048 Heksagonalnie - Klient ")

            
            
        except socket.error:
            QMessageBox.warning(self, "Error", "Żaden serwer nie został uruchomiony")

    def Czat_z_klientem(self):
        while(self.connected==True):
            text = input("Wiadomość do klienta: \n")
            mess = f"[Serwer]: {text}"
            self.Wyslij_do_klienta(mess)
            print(mess)

    def Czat_z_serwerem(self):
        while(self.connected == True):
            text = input("Wiadomość do serwera: \n")
            mess = f"[Klient]: {text} "
            self.Wyslij_do_serwera(mess)
            print(mess)


    def Wyslij_do_klienta(self, arg):
        try:
            self.serwer.sendall(bytes(arg,"utf-8"))
        except socket.error:
            return

    def Wyslij_do_serwera(self, arg):
        try:
            self.serwer.sendall(bytes(arg,"utf-8"))

        except socket.error:
            print("Utracono połączenie z serwerem!")
            return

    def Serwer_odbior(self, connection):
        while True:
            try:
                data = connection.recv(1024)
                if data != "":
                    rec = data.decode("utf-8")
                    self.ruch = True
                    self.connected = True
                    print(rec)
                    

            except socket.error:
                connection.close()
                self.connected = False
                return
        
    
    def Klient_odbior(self):
        while True:
            try:
                data = self.serwer.recv(1024)
                if data != "":
                    self.ruch = True
                    rec = data.decode("utf-8")
                print("kliiiiiiii")
                print(rec)

            except socket.error:
                print('Nastąpiło rozłączenie od serwera!')
                self.connected = False
                return



#  Metoda inicjująca nadanie wartości dla nowego (dotychczas pustego) pola
    def Add_Value (self,plansza,init):
        puste_pola=[]
        length = len(plansza)


        for j in range(0,length):
            if(plansza[j].value == None):
                puste_pola.append(j)

    
        x = random.randint(0,len(puste_pola)-1)
        if init == 0:
            plansza[puste_pola[x]].Pobierz_numer2()

        elif init == 2:
            self.plansze[0][puste_pola[x]].Pobierz_numer2()
            self.plansze[1][puste_pola[x]].Pobierz_numer2()

        else:
            plansza[puste_pola[x]].Pobierz_numer()

        return x

# Metoda rozpoczynająca grę 
    def Inicjuj (self, plansza):
        self.textedit.clear()
        self.finish = False
        self.result = 0
        self.Add_Value(plansza,0)
        self.Add_Value(plansza,0)
        
        for planszaa in self.plansze:
            self.Aktualizuj_Plansze(planszaa)

    def InicjujMultiplayer (self):
        self.textedit.clear()
        self.finish = False
        self.result = 0

        for plansza in self.plansze:
            self.Add_Value(plansza,2)
            
        
        for planszaa in self.plansze:
            self.Aktualizuj_Plansze(planszaa)

# Metoda resetująca całą planszę i inicjująca ją ponownie
    def Restart (self):
        self.finish = False
        self.result = 0
        self.dwuosobowa = False
        self.auto = False

        self.ruch = True
        for plansza in self.plansze:
            for pole in plansza:
                pole.Zeruj()
            self.score = 0
        self.Inicjuj(self.plansze[0])

    def RestartMultiplayer (self):
        self.dwuosobowa = True
        self.ruch = True
        self.auto = False

        self.finish = False
        self.result = 0
        for plansza in self.plansze:
            for pole in plansza:
                pole.Zeruj()
        self.score = 0
        self.InicjujMultiplayer()

    def Exit (self):
        self.connected = False
        self.close()

        

        

def main():
    run = True
    aplikacja = QApplication(sys.argv)

    okno = Okno()
    okno.Inicjuj(okno.plansze[0])

    sys.exit(aplikacja.exec_())



main()
